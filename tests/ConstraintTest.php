<?php

namespace Dropkick\Core\Constraint;

use PHPUnit\Framework\TestCase;

class ConstraintTest extends TestCase {
  public function testMissingRequired() {
    $this->expectException(\InvalidArgumentException::class);

    $constraint = new TestRequiredConstraint();
  }

  public function testRequired() {
    $constraint = new TestRequiredConstraint(['required' => 'required']);

    $this->assertTrue($constraint->hasOption('optional'));
    $this->assertEquals(TRUE, $constraint->getOption('optional'));
    $this->assertTrue($constraint->hasOption('required'));
    $this->assertEquals('required', $constraint->getOption('required', 'default'));
    $this->assertEquals('default', $constraint->getOption('default', 'default'));
  }

  public function testCreate() {
    $constraint = Constraint::create(TestRequiredConstraint::class, ['required' => 'required']);
    $this->assertTrue(is_object($constraint));
    $this->assertEquals(TestRequiredConstraint::class, get_class($constraint));
  }

  public function testGetSet() {
    $factory = Constraint::getFactory();
    $this->assertEquals(ConstraintFactory::class, get_class($factory));
    $this->assertNotEquals(TestConstraintFactory::class, get_class($factory));

    Constraint::setFactory(new TestConstraintFactory());
    $factory = Constraint::getFactory();
    $this->assertNotEquals(ConstraintFactory::class, get_class($factory));
    $this->assertEquals(TestConstraintFactory::class, get_class($factory));
  }

}
