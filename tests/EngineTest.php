<?php

namespace Dropkick\Core\Constraint;

use PHPUnit\Framework\TestCase;

class EngineTest extends TestCase {

  /**
   * @var \Dropkick\Core\Constraint\EngineInterface
   */
  protected $engine;

  public function setUp(): void {
    $this->engine = new Engine(new TestValidatorFactory());
  }

  public function testSimpleValueNoConstraints() {
    $value = new TestValue('unconstrained');
    $context = new Context($value);

    $this->engine->validate($value, $context);

    $this->assertEquals(0, count($context->getViolations()));
  }

  public function testSimpleValueSuccess() {
    $value = new TestValue('success');
    $context = new Context($value);

    $this->engine->validate($value, $context);

    $this->assertEquals(0, count($context->getViolations()));
  }
  
  public function testSimpleValueFailed() {
    $value = new TestValue('failed');
    $context = new Context($value);

    $this->engine->validate($value, $context);

    $violations = $context->getViolations();
    $this->assertEquals(1, count($violations));
    $this->assertEquals('validation failed', (string)$violations[0]->getMessage());
  }

  public function testComplexFailure() {
    $value = new TestComplexValue([
      'one' => new TestValue('success'),
      'two' => new TestValue('unconstrained'),
      'three' => new TestComplexValue([
        'failed' => new TestValue('failed'),
      ]),
    ]);

    $context = new Context($value);

    $this->engine->validate($value, $context);

    $violations = $context->getViolations();
    $this->assertEquals(1, count($violations));
    $this->assertEquals('validation failed', (string)$violations[0]->getMessage());
    $this->assertEquals('three.failed', $violations[0]->getPath());
  }

  public function testComplexConstraints() {
    $value = new TestComplexValue([
      'one' => new TestValue('success'),
      'two' => new TestValue('unconstrained'),
      'three' => new TestComplexValue([
        'failure' => new TestValue('success'),
      ], [], [new TestComplexConstraint()]),
    ]);

    $context = new Context($value);

    $this->engine->validate($value, $context);

    $violations = $context->getViolations();
    $this->assertEquals(1, count($violations));
    $this->assertEquals('complex validation', (string)$violations[0]->getMessage());
    $this->assertEquals('three', $violations[0]->getPath());
  }

}

