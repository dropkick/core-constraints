<?php

namespace Dropkick\Core\Constraint\Exception;

use Dropkick\Core\Constraint\Constraint;
use PHPUnit\Framework\TestCase;

class ValidatorUndefinedExceptionTest extends TestCase {
  public function testGetConstraint() {
    $constraint = new Constraint();
    $exception = new ValidatorUndefinedException($constraint);
    $this->assertEquals($constraint, $exception->getConstraint());
  }

}
