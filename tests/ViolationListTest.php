<?php

namespace Dropkick\Core\Constraint;

use Dropkick\Core\Constraint\Exception\OutOfBoundsException;
use PHPUnit\Framework\TestCase;

class ViolationListTest extends TestCase {

  public function testConstruction() {
    $value = new TestValue('success');
    $violation = new Violation('failed {{message}}', ['message' => 'message'], $value, '', 'failure');
    $list = new ViolationList([$violation]);

    $this->assertEquals(1, count($list));
    $this->assertEquals(1, $list->count());
    $this->assertTrue($list->has(0));
    $this->assertEquals($violation, $list->get(0));

    $list->remove(0);

    $this->assertEquals(0, count($list));
    $this->assertEquals(0, $list->count());
    $this->assertFalse($list->has(0));
  }

  public function testMerge() {
    $value = new TestValue('success');
    $violation = new Violation('failed {{message}}', ['message' => 'message'], $value, '', 'failure');
    $violation1 = new Violation('failed {{message}}', ['message' => 'violation'], $value, '', 'failure');
    $list = new ViolationList([$violation]);
    $merge = new ViolationList([$violation1]);

    $list->merge($merge);

    $this->assertEquals(2, count($list));
    $this->assertEquals(2, $list->count());
    $this->assertTrue($list->has(0));
    $this->assertTrue($list->has(1));
    $this->assertEquals($violation, $list->get(0));
    $this->assertEquals($violation1, $list->get(1));

    $expected = [$violation, $violation1];
    foreach ($list as $key => $value) {
      $this->assertEquals($expected[$key], $value);
    }

    $list->remove(0);

    $list->set(1, $violation);

    $this->assertEquals(1, count($list));
    $this->assertEquals(1, $list->count());
    $this->assertEquals($violation, $list->get(1));
  }

  public function testGetException() {
    $value = new TestValue('success');
    $violation = new Violation('failed {{message}}', ['message' => 'message'], $value, '', 'failure');
    $list = new ViolationList([$violation]);

    $this->expectException(OutOfBoundsException::class);
    $list->get(100);
  }

  public function testIsset() {
    $value = new TestValue('success');
    $violation = new Violation('failed {{message}}', ['message' => 'message'], $value, '', 'failure');
    $list = new ViolationList([$violation]);

    $this->assertTrue(isset($list[0]));
    $this->assertFalse(isset($list[1]));

    unset($list[0]);
    $this->assertFalse(isset($list[0]));

    $list[0] = $violation;
    $this->assertTrue(isset($list[0]));
  }

  public function testGetValue() {
    $value = new TestValue('success');
    $violation = new Violation('failed {{message}}', ['message' => 'message'], $value, '', 'failure');
    $list = new ViolationList([$violation]);

    $this->assertEquals($value, $list->get(0)->getRoot());
    $this->assertEquals('failure', $list->get(0)->getInvalidValue());
  }
}
