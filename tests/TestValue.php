<?php

namespace Dropkick\Core\Constraint;

class TestValue implements ValueInterface {

  protected $value;

  public function __construct($value) {
    $this->value = $value;
  }

  public function getValue() {
    return $this->value;
  }

  public function getConstraints() {
    if ($this->value === 'unconstrained') {
      return [];
    }
    return [new TestConstraint()];
  }
}