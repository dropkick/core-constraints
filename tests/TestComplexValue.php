<?php

namespace Dropkick\Core\Constraint;

class TestComplexValue implements ComplexValueInterface {

  /**
   * @var \Dropkick\Core\Constraint\ValueInterface[]
   */
  protected $children;

  /**
   * @var \Dropkick\Core\Constraint\ConstraintInterface[]
   */
  protected $constraints;

  /**
   * @var \Dropkick\Core\Constraint\ConstraintInterface[]
   */
  protected $childConstraints;


  public function __construct($children, $constraints = [], $child_constraints = []) {
    $this->children = $children;
    $this->constraints = $constraints;
    $this->childConstraints = $child_constraints;
  }

  public function getChildConstraints() {
    return $this->childConstraints;
  }

  public function getChildren() {
    return array_keys($this->children);
  }

  public function getChild($child) {
    return $this->children[$child];
  }

  public function getValue() {
    $list = [];
    foreach ($this->children as $key => $value) {
      $list[$key] = $value->getValue();
    }
    return $list;
  }

  public function getConstraints() {
    return $this->constraints;
  }
}