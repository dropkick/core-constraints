<?php

namespace Dropkick\Core\Constraint;

class TestValidator extends Validator {

  public function validate($value, ConstraintInterface $constraint, ContextInterface $context) {
    if ($value !== 'success') {
      $context->addViolation('validation {{ value }}', ['value' => $value]);
    }
  }
}