<?php

namespace Dropkick\Core\Constraint;

use PHPUnit\Framework\TestCase;

class ValidatorTest extends TestCase {

  public function testFactory() {
    $factory = Validator::getDefaultFactory();
    $this->assertEquals(ValidatorFactory::class, get_class($factory));

    Validator::setDefaultFactory(new TestValidatorFactory());
    $factory = Validator::getDefaultFactory();
    $this->assertEquals(TestValidatorFactory::class, get_class($factory));
  }

}
