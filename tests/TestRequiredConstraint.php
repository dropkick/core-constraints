<?php

namespace Dropkick\Core\Constraint;


class TestRequiredConstraint extends Constraint {

  protected function getDefaultOptions() {
    return parent::getDefaultOptions() + [
      'optional' => TRUE,
    ];
  }

  protected function getRequiredOptions() {
    return parent::getRequiredOptions() + ['required'];
  }

}
