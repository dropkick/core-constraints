<?php

namespace Dropkick\Core\Constraint;

use PHPUnit\Framework\TestCase;

class IndexPathTest extends TestCase {

  public function testIndexs() {
    $index = new IndexPath('three.valid.parts');
    $indexes = $index->getIndexes();
    $this->assertEquals(3, count($indexes));
    $this->assertEquals('three', $indexes[0]);
    $this->assertEquals('valid', $indexes[1]);
    $this->assertEquals('parts', $indexes[2]);
  }

}
