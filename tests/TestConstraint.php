<?php

namespace Dropkick\Core\Constraint;

class TestConstraint extends Constraint {

  public function getValidator() {
    return TestValidator::class;
  }
}