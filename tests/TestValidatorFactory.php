<?php

namespace Dropkick\Core\Constraint;

class TestValidatorFactory implements ValidatorFactoryInterface {

  public function createValidator(ConstraintInterface $constraint) {
    $validator = $constraint->getValidator();
    return new $validator();
  }
}