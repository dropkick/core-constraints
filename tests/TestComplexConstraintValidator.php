<?php

namespace Dropkick\Core\Constraint;

class TestComplexConstraintValidator extends Validator {

  public function validate($value, ConstraintInterface $constraint, ContextInterface $context) {
    if (!isset($value['success'])) {
      $context->addViolation('complex validation');
    }
  }
}