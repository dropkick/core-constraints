<?php

namespace Dropkick\Core\Constraint;

use Dropkick\Core\Constraint\Exception\ValidatorUndefinedException;
use PHPUnit\Framework\TestCase;

class ValidatorFactoryTest extends TestCase {
  public function testInvalidValidator() {
    $this->expectException(ValidatorUndefinedException::class);

    $factory = new ValidatorFactory();
    $factory->createValidator(new TestInvalidValidatorConstraint());
  }

  public function testCreateValidator() {
    $factory = new ValidatorFactory();
    $validator = $factory->createValidator(new TestValidValidatorConstraint());
    $this->assertEquals(TestValidValidatorConstraintValidator::class, get_class($validator));
  }

}

class TestInvalidValidatorConstraint extends Constraint {
}

class TestValidValidatorConstraint extends Constraint {

}

class TestValidValidatorConstraintValidator extends Validator {
  public function validate($value, ConstraintInterface $constraint, ContextInterface $context) {
  }
}
