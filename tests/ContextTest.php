<?php

namespace Dropkick\Core\Constraint;

use PHPUnit\Framework\TestCase;

class ContextTest extends TestCase {

  public function testPath() {
    $value = new TestValue('success');

    $test = new Context($value);
    $test->push('three', new TestValue('three'));
    $test->push('failed', new TestValue('failed'));
    $test->push('parts', new TestValue('parts'));

    $this->assertEquals('three.failed.parts', $test->getIndexPath()->get());
    $this->assertEquals($value, $test->getRoot());
    $this->assertEquals('parts', $test->getValue()->getValue());
  }

}
