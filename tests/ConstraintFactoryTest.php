<?php

namespace Dropkick\Core\Constraint;

use Dropkick\Core\Constraint\Exception\ConstraintUndefinedException;
use PHPUnit\Framework\TestCase;

class ConstraintFactoryTest extends TestCase {

  public function testException() {
    $factory = new ConstraintFactory();

    $this->expectException(ConstraintUndefinedException::class);
    $factory->createConstraint(self::class . 'Constraint');
  }

  public function testConstraint() {
    $factory = new ConstraintFactory();

    $constraint = $factory->createConstraint(TestConstraint::class);
    $this->assertEquals(TestConstraint::class, get_class($constraint));
  }
}
