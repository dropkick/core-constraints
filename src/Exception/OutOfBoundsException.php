<?php

namespace Dropkick\Core\Constraint\Exception;

/**
 * Class OutOfBoundsException.
 *
 * Trigger when a violation list offset is out of bounds.
 */
class OutOfBoundsException extends \Exception {

}
