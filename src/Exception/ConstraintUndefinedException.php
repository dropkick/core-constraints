<?php

namespace Dropkick\Core\Constraint\Exception;

/**
 * Class ConstraintUndefinedException.
 *
 * Triggered when a Constraint was requested from the factory, but none was
 * located.
 */
class ConstraintUndefinedException extends \Exception {
}
