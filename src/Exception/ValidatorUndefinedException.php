<?php

namespace Dropkick\Core\Constraint\Exception;

use Dropkick\Core\Constraint\ConstraintInterface;

/**
 * Class ValidatorUndefinedException.
 *
 * This is triggered when a validator factory cannot locate an appropriate
 * validator for the constraint.
 */
class ValidatorUndefinedException extends \Exception {

  /**
   * The constraint that caused the exception.
   *
   * @var \Dropkick\Core\Constraint\ConstraintInterface
   */
  protected $constraint;

  /**
   * ValidatorUndefinedException constructor.
   *
   * @param \Dropkick\Core\Constraint\ConstraintInterface $constraint
   *   The constraint causing the invalid validator.
   * @param string $message
   *   The exception message.
   * @param int $code
   *   The exception code.
   * @param \Throwable|null $previous
   *   The previous exception.
   */
  public function __construct(ConstraintInterface $constraint, $message = "", $code = 0, \Throwable $previous = NULL) {
    parent::__construct($message, $code, $previous);
    $this->constraint = $constraint;
  }

  /**
   * Return the constraint object that caused the validator exception.
   *
   * @return \Dropkick\Core\Constraint\ConstraintInterface
   *   The constraint object.
   */
  public function getConstraint() {
    return $this->constraint;
  }

}
