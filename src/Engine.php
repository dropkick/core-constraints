<?php

namespace Dropkick\Core\Constraint;

/**
 * Class Engine.
 *
 * A generic implementation of the EngineInterface.
 */
class Engine implements EngineInterface {

  /**
   * The validator creation factory.
   *
   * @var \Dropkick\Core\Constraint\ValidatorFactoryInterface
   */
  protected $factory;

  /**
   * Engine constructor.
   *
   * @param \Dropkick\Core\Constraint\ValidatorFactoryInterface $factory
   *   The validator creation factory.
   */
  public function __construct(ValidatorFactoryInterface $factory) {
    $this->factory = $factory;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(ValueInterface $value, ContextInterface $context) {
    // Process the child constrained values if they exist.
    if ($value instanceof ComplexValueInterface) {
      // Process any pre-child constraints.
      foreach ($value->getChildConstraints() as $constraint) {
        $this->factory
          ->createValidator($constraint)
          ->validate($value->getValue(), $constraint, $context);
      }

      // Process any child constraints.
      foreach ($value->getChildren() as $child) {
        $child_value = $value->getChild($child);
        $context->push($child, $child_value);
        $this->validate($child_value, $context);
        $context->pop();
      }
    }

    // Process each constraint for the value.
    foreach ($value->getConstraints() as $constraint) {
      $this->factory
        ->createValidator($constraint)
        ->validate($value->getValue(), $constraint, $context);
    }
  }

}
