<?php

namespace Dropkick\Core\Constraint;

use Dropkick\Core\Constraint\Exception\ConstraintUndefinedException;
use Dropkick\Core\Formattable\FormattableString;

/**
 * Class ConstraintFactory.
 *
 * A generic implementation of ConstraintFactoryInterface.
 */
class ConstraintFactory implements ConstraintFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function createConstraint($constraint, array $options = []) {
    if (!class_exists($constraint)) {
      throw new ConstraintUndefinedException(
        FormattableString::create(
          'Unable to locate the constraint class "{{ constraint }}".',
          ['constraint' => $constraint]
        )
      );
    }
    return new $constraint($options);
  }

}
