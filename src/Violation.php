<?php

namespace Dropkick\Core\Constraint;

use Dropkick\Core\Formattable\FormattableString;

/**
 * Class Violation.
 *
 * A generic implementation of the ViolationInterface.
 */
class Violation implements ViolationInterface {

  /**
   * The message.
   *
   * @var \Dropkick\Core\Formattable\FormattableInterface
   */
  protected $message;

  /**
   * The root value.
   *
   * @var \Dropkick\Core\Constraint\ValueInterface
   */
  protected $root;

  /**
   * The path from the root value to the data that caused the violation.
   *
   * @var string
   */
  protected $path;

  /**
   * The invalid value.
   *
   * @var mixed
   */
  protected $value;

  /**
   * Violation constructor.
   *
   * @param string $message
   *   The violation message.
   * @param array $params
   *   The violation message parameters.
   * @param \Dropkick\Core\Constraint\ValueInterface $root
   *   The root value being validated.
   * @param string $path
   *   The path that arrives at value being validated.
   * @param mixed $value
   *   The invalid value that caused the violation.
   */
  public function __construct($message, array $params, ValueInterface $root, $path, $value) {
    $this->message = FormattableString::create($message, $params);
    $this->root = $root;
    $this->path = $path;
    $this->value = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoot() {
    return $this->root;
  }

  /**
   * {@inheritdoc}
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * {@inheritdoc}
   */
  public function getInvalidValue() {
    return $this->value;
  }

}
