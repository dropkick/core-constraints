<?php

namespace Dropkick\Core\Constraint;

/**
 * Interface ValidatorFactoryInterface.
 *
 * Constraints require the validators to perform the actual validation.
 */
interface ValidatorFactoryInterface {

  /**
   * The validator for the constraint.
   *
   * @param \Dropkick\Core\Constraint\ConstraintInterface $constraint
   *   The constraint needing a validator.
   *
   * @return \Dropkick\Core\Constraint\ValidatorInterface
   *   The appropriate validator.
   *
   * @throws \Dropkick\Core\Constraint\Exception\ValidatorUndefinedException
   *   When there is no validator available for the constraint.
   */
  public function createValidator(ConstraintInterface $constraint);

}
