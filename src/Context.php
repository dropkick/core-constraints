<?php

namespace Dropkick\Core\Constraint;

/**
 * Class Context.
 *
 * A generic implementation of ContextInterface.
 */
class Context implements ContextInterface {

  /**
   * The violations list.
   *
   * @var \Dropkick\Core\Constraint\ViolationList
   */
  protected $violations;

  /**
   * The root value.
   *
   * @var \Dropkick\Core\Constraint\ValueInterface
   */
  protected $root;

  /**
   * The index path.
   *
   * @var \Dropkick\Core\Constraint\IndexPath
   */
  protected $path;

  /**
   * The current value.
   *
   * @var \Dropkick\Core\Constraint\ValueInterface
   */
  protected $current;

  /**
   * The list of values currently being validated.
   *
   * @var \Dropkick\Core\Constraint\ValueInterface[]
   */
  protected $stack = [];

  /**
   * Context constructor.
   *
   * @param \Dropkick\Core\Constraint\ValueInterface $root
   *   The root value of the context.
   */
  public function __construct(ValueInterface $root) {
    $this->violations = new ViolationList();
    $this->path = new IndexPath();
    $this->root = $root;
    $this->current = $root;
  }

  /**
   * {@inheritdoc}
   */
  public function addViolation($message, array $params = []) {
    $this->violations->add(new Violation($message, $params, $this->root, $this->path->get(), $this->current->getValue()));
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getViolations() {
    return $this->violations;
  }

  /**
   * {@inheritdoc}
   */
  public function getRoot() {
    return $this->root;
  }

  /**
   * {@inheritdoc}
   */
  public function getValue() {
    return $this->current;
  }

  /**
   * {@inheritdoc}
   */
  public function push($index, ValueInterface $value) {
    $this->path->push($index);
    $this->stack[] = $this->current;
    $this->current = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function pop() {
    if (count($this->stack)) {
      $this->path->pop();
      $value = array_pop($this->stack);
      $this->current = $value;
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getIndexPath() {
    return $this->path;
  }

}
