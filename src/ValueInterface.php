<?php

namespace Dropkick\Core\Constraint;

/**
 * Interface ValueInterface.
 *
 * The basis of validating values is that they can have constraints.
 */
interface ValueInterface {

  /**
   * Return the value.
   *
   * @return mixed
   *   The values value.
   */
  public function getValue();

  /**
   * Get the constraints that apply to this value.
   *
   * @return \Dropkick\Core\Constraint\ConstraintInterface[]
   *   The constraints.
   */
  public function getConstraints();

}
