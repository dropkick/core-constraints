<?php

namespace Dropkick\Core\Constraint;

/**
 * Class Validator.
 *
 * The validator is entirely responsible for handling any complex data models
 * to mix this validator is being applied. This only applies for constrained
 * values, as unconstrained values are not restricted, and therefore do not
 * require validation.
 *
 * A complex value is only unconstrained if all elements within it are
 * unconstrained.
 */
abstract class Validator implements ValidatorInterface {

  /**
   * The validator factory used when generating validators for constraints.
   *
   * @var \Dropkick\Core\Constraint\ValidatorFactoryInterface
   */
  protected static $defaultFactory;

  /**
   * Get the default validator factory.
   *
   * @return \Dropkick\Core\Constraint\ValidatorFactoryInterface
   *   The validator factory.
   */
  public static function getDefaultFactory() {
    if (!isset(self::$defaultFactory)) {
      self::$defaultFactory = new ValidatorFactory();
    }
    return self::$defaultFactory;
  }

  /**
   * Set the default validator factory.
   *
   * @param \Dropkick\Core\Constraint\ValidatorFactoryInterface $factory
   *   The validator factory.
   */
  public static function setDefaultFactory(ValidatorFactoryInterface $factory) {
    self::$defaultFactory = $factory;
  }

}
