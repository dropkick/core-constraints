<?php

namespace Dropkick\Core\Constraint;

/**
 * Interface ViolationListInterface.
 *
 * Contains a list of all the violations that occurred when validating against
 * the constraints.
 */
interface ViolationListInterface extends \Traversable, \Countable, \ArrayAccess {

  /**
   * Add a violation to the list.
   *
   * @param \Dropkick\Core\Constraint\ViolationInterface $violation
   *   The violation object.
   *
   * @return static
   *   The violation list object.
   */
  public function add(ViolationInterface $violation);

  /**
   * Merges the other list into this list.
   *
   * @param \Dropkick\Core\Constraint\ViolationListInterface $otherList
   *   A violation list object.
   *
   * @return static
   *   The violation list object.
   */
  public function merge(ViolationListInterface $otherList);

  /**
   * Get the violation at the offset.
   *
   * @param int $offset
   *   The offset of the violation.
   *
   * @return \Dropkick\Core\Constraint\ViolationInterface
   *   The violation object.
   *
   * @throws \Dropkick\Core\Constraint\Exception\OutOfBoundsException
   */
  public function get($offset);

  /**
   * Confirm the existence of the offset.
   *
   * @param int $offset
   *   The offset to check.
   *
   * @return bool
   *   Confirmation of the existence of the offset.
   */
  public function has($offset);

  /**
   * Sets a violation at a given offset.
   *
   * @param int $offset
   *   The offset to set.
   * @param \Dropkick\Core\Constraint\ViolationInterface $violation
   *   The violation that occurred.
   *
   * @return static
   *   The violation list object.
   */
  public function set($offset, ViolationInterface $violation);

  /**
   * Removes a violation at a given offset.
   *
   * @param int $offset
   *   The offset to remove.
   *
   * @return static
   *   The violation list object.
   */
  public function remove($offset);

}
