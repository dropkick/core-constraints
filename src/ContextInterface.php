<?php

namespace Dropkick\Core\Constraint;

/**
 * Interface ContextInterface.
 *
 * Contains the context of the validation chain.
 */
interface ContextInterface {

  /**
   * Adds a violation for the current value object.
   *
   * @param string $message
   *   The message string.
   * @param array $params
   *   The message parameters.
   *
   * @return static
   *   The context object.
   */
  public function addViolation($message, array $params = []);

  /**
   * Get the violations that have occurred while validating.
   *
   * @return \Dropkick\Core\Constraint\ViolationListInterface
   *   The violation list.
   */
  public function getViolations();

  /**
   * Get the root of the validation context.
   *
   * The current value is returned by {@link getValue}.
   *
   * @return \Dropkick\Core\Constraint\ValueInterface
   *   The root value being validated.
   */
  public function getRoot();

  /**
   * Get the value that the validator is currently validating.
   *
   * @return \Dropkick\Core\Constraint\ValueInterface
   *   The value being validated.
   */
  public function getValue();

  /**
   * Push an index into the index path.
   *
   * @param string $index
   *   The index of the value to be validated.
   * @param \Dropkick\Core\Constraint\ValueInterface $value
   *   The value to be validated.
   *
   * @return static
   *   The context object.
   */
  public function push($index, ValueInterface $value);

  /**
   * Remove an index from the index path.
   *
   * @return static
   *   The context object.
   */
  public function pop();

  /**
   * Get the index path from the root object.
   *
   * @return \Dropkick\Core\Constraint\IndexPath
   *   The index path.
   */
  public function getIndexPath();

}
