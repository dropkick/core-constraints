<?php

namespace Dropkick\Core\Constraint;

/**
 * Interface ComplexValueInterface.
 *
 * This is when the complex value contains children to validate as well.
 */
interface ComplexValueInterface extends ValueInterface {

  /**
   * Return the list of constraints before processing the child constraints.
   *
   * @return \Dropkick\Core\Constraint\ConstraintInterface[]
   *   The constraints.
   */
  public function getChildConstraints();

  /**
   * Return the list of constrained children.
   *
   * @return string[]
   *   The child indexes that are constrained.
   */
  public function getChildren();

  /**
   * Return the constrained child value.
   *
   * @param string $child
   *   The child index.
   *
   * @return \Dropkick\Core\Constraint\ValueInterface
   *   The constrained child value.
   */
  public function getChild($child);

}
