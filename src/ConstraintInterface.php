<?php

namespace Dropkick\Core\Constraint;

/**
 * Interface ConstraintInterface.
 *
 * Configuration for a validation option that can be applied to a value.
 */
interface ConstraintInterface {

  /**
   * Confirm the constraint has an option.
   *
   * @param string $key
   *   The option key.
   *
   * @return bool
   *   Confirmation the option exists.
   */
  public function hasOption($key);

  /**
   * Return an option value.
   *
   * @param string $key
   *   The option key.
   * @param mixed $default
   *   A default value.
   *
   * @return mixed
   *   The option value, or default if undefined.
   */
  public function getOption($key, $default = NULL);

  /**
   * Returns the validator class name.
   *
   * @return string
   *   The validator class name.
   */
  public function getValidator();

}
