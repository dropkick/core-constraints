<?php

namespace Dropkick\Core\Constraint;

/**
 * Interface ConstraintFactoryInterface.
 *
 * The factory for creating constraints.
 */
interface ConstraintFactoryInterface {

  /**
   * Create a constraint object.
   *
   * @param string $constraint
   *   The requested constraint.
   * @param array $options
   *   The constraint options.
   *
   * @return \Dropkick\Core\Constraint\ConstraintInterface
   *   The constraint object.
   *
   * @throws \Dropkick\Core\Constraint\Exception\ConstraintUndefinedException
   *   Triggered when no valid constraint could be located.
   */
  public function createConstraint($constraint, array $options = []);

}
