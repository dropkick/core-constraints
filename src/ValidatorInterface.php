<?php

namespace Dropkick\Core\Constraint;

/**
 * Interface ValidatorInterface.
 *
 * A validator uses a constraint to confirm that the value matches the
 * expected set of allowable values. In the case of complex data models,
 * these perform validations on each of the children of the data model.
 */
interface ValidatorInterface {

  /**
   * Checks if the passed value is valid.
   *
   * Values which are invalid, add a violation to the context.
   *
   * @param mixed $value
   *   The value to validate.
   * @param \Dropkick\Core\Constraint\ConstraintInterface $constraint
   *   The constraint used to validate the value.
   * @param \Dropkick\Core\Constraint\ContextInterface $context
   *   The context of the validation.
   */
  public function validate($value, ConstraintInterface $constraint, ContextInterface $context);

}
