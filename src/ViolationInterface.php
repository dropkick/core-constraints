<?php

namespace Dropkick\Core\Constraint;

/**
 * Interface ViolationInterface.
 *
 * A violation of a constraint when validating values.
 */
interface ViolationInterface {

  /**
   * Returns the violation message.
   *
   * @return \Dropkick\Core\Formattable\FormattableInterface
   *   The violation message.
   */
  public function getMessage();

  /**
   * Returns the root element of the validation.
   *
   * @return \Dropkick\Core\Constraint\ValueInterface
   *   The value at the root of the validation.
   */
  public function getRoot();

  /**
   * Returns the index path from the root element to the violation.
   *
   * @return string
   *   The index path is a colon separated sequence of indexes that
   *   are used to progress from the root element to the element
   *   that caused the violation.
   */
  public function getPath();

  /**
   * Returns the value that caused the violation.
   *
   * @return mixed
   *   The value that was invalid.
   */
  public function getInvalidValue();

}
