<?php

namespace Dropkick\Core\Constraint;

/**
 * Class IndexPath.
 *
 * Provides the management of the index path behaviour for recursive
 * constraints.
 */
class IndexPath {

  /**
   * The indexes as an array.
   *
   * @var string[]
   */
  protected $indexes = [];

  /**
   * IndexPath constructor.
   *
   * @param array|string $indexes
   *   The indexes as a string, or an array of indexes.
   */
  public function __construct($indexes = []) {
    $indexes = is_string($indexes) ? explode('.', $indexes) : (array) $indexes;
    foreach ($indexes as $index) {
      $this->push($index);
    }
  }

  /**
   * Add an index to the path.
   *
   * @param string $index
   *   The index.
   *
   * @return static
   *   The index path object.
   */
  public function push($index) {
    $this->indexes[] = $index;
    return $this;
  }

  /**
   * Remove an index from the path.
   *
   * @return static
   *   The index path object.
   */
  public function pop() {
    array_pop($this->indexes);
    return $this;
  }

  /**
   * Get the indexes for the index path.
   *
   * @return string[]
   *   The indexes.
   */
  public function getIndexes() {
    return $this->indexes;
  }

  /**
   * Get the index path as a string.
   *
   * @return string
   *   The fully indexed path.
   */
  public function get() {
    return implode('.', $this->indexes);
  }

}
