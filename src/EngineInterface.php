<?php

namespace Dropkick\Core\Constraint;

/**
 * Interface EngineInterface.
 *
 * Performs validation on a value.
 */
interface EngineInterface {

  /**
   * Validates a constrained value.
   *
   * There is an expectation that any ConstrainedComplexValueInterface will be
   * processed after the child values.
   *
   * @param \Dropkick\Core\Constraint\ValueInterface $value
   *   The value to be validated.
   * @param \Dropkick\Core\Constraint\ContextInterface $context
   *   The context for the validation.
   */
  public function validate(ValueInterface $value, ContextInterface $context);

}
