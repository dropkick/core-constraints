<?php

namespace Dropkick\Core\Constraint;

use Dropkick\Core\Constraint\Exception\ValidatorUndefinedException;
use Dropkick\Core\Formattable\FormattableString;

/**
 * Class ValidatorFactory.
 *
 * A generic implementation of a validator factory.
 */
class ValidatorFactory implements ValidatorFactoryInterface {

  /**
   * {@inheritdoc}
   */
  public function createValidator(ConstraintInterface $constraint) {
    $validator = $constraint->getValidator();
    if (!class_exists($validator)) {
      throw new ValidatorUndefinedException(
        $constraint,
        FormattableString::create(
          'The constraint "{{ constraint }}" does not have a validator.',
          ['constraint' => get_class($constraint)]
        )
      );
    }
    return new $validator();
  }

}
