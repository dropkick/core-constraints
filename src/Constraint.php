<?php

namespace Dropkick\Core\Constraint;

use Dropkick\Core\Formattable\FormattableString;

/**
 * Class Constraint.
 *
 * A generic implementation of ConstraintInterface, modelled on symfony's
 * validation constraint.
 */
class Constraint implements ConstraintInterface {

  /**
   * The factory used to create constraints.
   *
   * @var \Dropkick\Core\Constraint\ConstraintFactoryInterface
   */
  protected static $factory;

  /**
   * The option values for the constraint.
   *
   * @var array
   */
  protected $options;

  /**
   * Constraint constructor.
   *
   * @param array $options
   *   The option values.
   */
  public function __construct(array $options = []) {
    // Save the options along with the defaults.
    $this->options = $options + $this->getDefaultOptions();

    // Confirm required options.
    foreach ($this->getRequiredOptions() as $required) {
      if (!$this->hasOption($required)) {
        throw new \InvalidArgumentException(
          FormattableString::create(
            'Constraint "{{ constraint }}" is missing the required option "{{ required }}".',
            ['constraint' => get_class($this), 'required' => $required]
          )
        );
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function hasOption($key) {
    return array_key_exists($key, $this->options);

  }

  /**
   * {@inheritdoc}
   */
  public function getOption($key, $default = NULL) {
    if (array_key_exists($key, $this->options)) {
      return $this->options[$key];
    }
    return $default;
  }

  /**
   * Return the list of required option keys.
   *
   * @return string[]
   *   The list of required option keys.
   */
  protected function getRequiredOptions() {
    return [];
  }

  /**
   * Return the default values for options.
   *
   * @return array
   *   The default options with key->value mapping.
   */
  protected function getDefaultOptions() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getValidator() {
    return get_class($this) . 'Validator';
  }

  /**
   * Create a constraint.
   *
   * @param string $constraint
   *   The identifier of the constraint.
   * @param array $options
   *   The options for the constraint.
   *
   * @return \Dropkick\Core\Constraint\ConstraintInterface
   *   The created conatraint.
   */
  public static function create($constraint, array $options = []) {
    return self::getFactory()->createConstraint($constraint, $options);
  }

  /**
   * Return the factory object for creating constraints.
   *
   * @return \Dropkick\Core\Constraint\ConstraintFactoryInterface
   *   The constraint factory object.
   */
  public static function getFactory() {
    if (!isset(self::$factory)) {
      self::$factory = new ConstraintFactory();
    }
    return self::$factory;
  }

  /**
   * Set the constraint factory object.
   *
   * @param \Dropkick\Core\Constraint\ConstraintFactoryInterface $factory
   *   The factory object.
   */
  public static function setFactory(ConstraintFactoryInterface $factory) {
    self::$factory = $factory;
  }

}
