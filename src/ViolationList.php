<?php

namespace Dropkick\Core\Constraint;

use Dropkick\Core\Constraint\Exception\OutOfBoundsException;
use Dropkick\Core\Formattable\FormattableString;

/**
 * Class ViolationList.
 *
 * A generic implementation of ViolationListInterface.
 */
class ViolationList implements \IteratorAggregate, ViolationListInterface {

  /**
   * The list of violations.
   *
   * @var \Dropkick\Core\Constraint\ViolationInterface[]
   */
  protected $violations = [];

  /**
   * ViolationList constructor.
   *
   * @param \Dropkick\Core\Constraint\ViolationInterface[] $violations
   *   The violations to initialize.
   */
  public function __construct(array $violations = []) {
    foreach ($violations as $violation) {
      $this->add($violation);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function add(ViolationInterface $violation) {
    $this->violations[] = $violation;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function merge(ViolationListInterface $otherList) {
    foreach ($otherList as $violation) {
      $this->add($violation);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function get($offset) {
    if (!isset($this->violations[$offset])) {
      throw new OutOfBoundsException(
        FormattableString::create(
          'The offset "{{ offset }}" does not exist.',
          ['offset' => $offset]
        )
      );
    }
    return $this->violations[$offset];
  }

  /**
   * {@inheritdoc}
   */
  public function has($offset) {
    return isset($this->violations[$offset]);
  }

  /**
   * {@inheritdoc}
   */
  public function set($offset, ViolationInterface $violation) {
    $this->violations[$offset] = $violation;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function remove($offset) {
    unset($this->violations[$offset]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function count() {
    return count($this->violations);
  }

  /**
   * {@inheritdoc}
   *
   * @return \ArrayIterator|\Dropkick\Core\Constraint\ViolationInterface[]
   *   The list iterator.
   */
  public function getIterator() {
    return new \ArrayIterator($this->violations);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetExists($offset) {
    return $this->has($offset);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetGet($offset) {
    return $this->get($offset);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetSet($offset, $value) {
    $this->set($offset, $value);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetUnset($offset) {
    $this->remove($offset);
  }

}
